package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        String returnString = "";
        for (String string : args) {
            returnString += string + " ";
        }
        return returnString;
    }

    @Override
    public String getName() {
        return "echo";
    }

    @Override
    public String getManual() {
        return "Returns following String to terminal. Like an echo-cho-cho";
    }
    
}
