package no.uib.inf101.terminal;

import java.util.Map;

public interface Command {
    /** Method that executes function with
     * @param args
     */
    String run(String[] args);

    /** @return Command name */
    String getName();

    /** Setter for public context
     * @param context
     */
    default void setContext(Context context) {/* do nothing */}
    
    /** Manual for command */
    String getManual();

    default void setCommandContext(Map<String, Command> commandMap){
    }
}
