package no.uib.inf101.terminal;

import java.util.Map;

public class CmdMan implements Command {
    
    private Map<String, Command> commandMap;

    @Override
    public void setCommandContext(Map<String, Command> commandMap){
        this.commandMap = commandMap;
    }
    
    @Override
    public String run(String[] args) {
        String commandName = args[0];
        Command command = commandMap.get(commandName);
        if (command != null) {
            return command.getManual();
        } else {
            return "Command not found";
        }
    }

    @Override
    public String getName() {
        return "man";
    }

    @Override
    public String getManual() {
        return "Returns manual of following command";
    }
    
}
