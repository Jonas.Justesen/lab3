package no.uib.inf101.terminal;

public class CmdPwd implements Command {

    Context context;

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public String run(String[] args) {
        return this.context.getCwd().getAbsolutePath();
    }

    @Override
    public String getName() {
        return "pwd";
    }

    @Override
    public String getManual() {
        return "Prints out current working directory";
    }
    
}
